const router = require('express').Router();
const pool = require('../database/db');

//Get all data
router.get('/',  async (req, res) => {
    try{
        const data = await pool.query('SELECT * FROM contacts')
        res.json({
            data : {
                messages: data.rows,
            }
        })
    } catch(error ) {
        console.error(error)

    }
});
//Get individual data
router.get('/:id', async (req, res) => {
    const {id} = req.params;
    try{
        const data = await pool.query("SELECT * FROM contacts WHERE id = $1", [id]);
        res.json(
            {
                data : {
                    message: data.rows[0],
                }
            })
    } catch(err){
        console.error(err)
    }
});

router.post('/', async (req, res) => {
   
    try{
        const{name, number, message, email} = req.body;

        if(number.length < 8){
            return res.status(401).json('Number cannot be less than 8');
        };


        const data = await pool.query("INSERT INTO contacts(name, number, message, email) VALUES ($1, $2, $3, $4)", [name, number, message, email]);
        
        
        
        res.json({
            data : {
                message: data.rows[0],
            }
        });

        
    } catch(err){
        console.error(err)
    }
});

//Delete Data
router.delete('/:id', async(req, res) => {
    try {
        const data = await pool.query("DELETE FROM contacts WHERE id = $1", [req.params.id]);
        res.json({
            data : {
                message: data.rows[0],
            }
        })
    }catch(err){
        console.error(err)
    }
});

module.exports = router;