const router = require('express').Router();
const pool = require('../database/db');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, './gallery');
      }, // Uploads folder
    filename: (req, file, cb) => {
        cb(null, `gallery-${file.originalname}`); // Use original name for the uploaded file
    }
});

const imgfilter = (req, file,callback) => {
    if(file.mimetype.startsWith('gallery')) {
      callback(null, true)
  }else{
    callback(null, Error('only gallery is allowed'))
  }};


  const upload = multer({ 
    storage: storage,
  fileFilter: imgfilter
   });

   router.get('/',  async (req, res) => {
    try{
        const data = await pool.query('SELECT * FROM gallery')
        res.json({
            data : {
                images: data.rows,
            }
        })
    } catch(error ) {
        console.error(error)

    }
});
//Get individual data
router.get('/:id', async (req, res) => {
    const {id} = req.params;
    try{
        const data = await pool.query("SELECT * FROM gallery WHERE id = $1", [id]);
        res.json(
            {
                data : {
                    image: data.rows[0],
                }
            })
    } catch(err){
        console.error(err)
    }
});

//Create Data
router.post('/', upload.single('image'), async (req, res) => {
    const { filename } = req.file;
    try{
        const data = await pool.query("INSERT INTO gallery(image) VALUES ($1)", [filename]);
        res.json({
            data : {
                image: data.rows[0],
            }
        })
    } catch(err){
        console.error(err)
    }
});

router.delete('/:id', async(req, res) => {
    try {
        const data = await pool.query("DELETE FROM gallery WHERE id = $1", [req.params.id]);
        res.json({
            data : {
                image: data.rows[0],
            }
        })
    }catch(err){
        console.error(err)
    }
});

module.exports = router;