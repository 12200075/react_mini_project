import { useEffect, useState } from "react";
import { toast } from 'react-toastify';


const ImageDetails = () => {
  const[images, setImages] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try{
        const response = await fetch('http://localhost:8000/gallery', {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          }
          });
          const data = await response.json()
            setImages(data.data.images)
 
      }catch(err) {
        console.log(err)
      }
    }
    fetchData();   
  },[]);


  const handleDelete = async(id) => {
    try {
      const response = await fetch(`http://localhost:8000/gallery/${id}`, {
        method: 'DELETE'
      });
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      setImages(images.filter(image => image.id !== id));
      toast.success('Image Delete Successfully', {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        });
    } catch(err) {
      console.error(err);
    }
  };
  
 
    return (
  <>
    
    <table className="datatable">
        <thead>
            <tr>
                <th scope="col">Image</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
          {images && images.map((image) => {
            return (
              <tr key={image.id}>
              <td data-label='Image'><img src={`http://localhost:8000/gallery/${image.image}`} alt="Pet" /></td>
              <td data-label='Delete'><button className="delete_btn" onClick={() => handleDelete(image.id)} >Delete</button></td>
            </tr>
            )          
          })}         
        </tbody>
    </table>
</>
    );
  }
  export default ImageDetails;