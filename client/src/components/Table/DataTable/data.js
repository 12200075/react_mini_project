import { useContext, useEffect } from "react";
import { PetifyContext } from "../../../context/context";
import {useNavigate} from 'react-router-dom';
import { toast } from 'react-toastify';
import "./DataTable.css";


const DataDetails = () => {
  const {pets, setPets} = useContext(PetifyContext);
  let navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      try{
        const response = await fetch('http://localhost:8000/data', {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          }
          });
          const data = await response.json()
        setPets(data.data.pets)
 
      }catch(err) {
        console.log(err)
      }
    }
    fetchData();   
  },[]);


  const handleDelete = async(id) => {
    try {
      const response = await fetch(`http://localhost:8000/data/${id}`, {
        method: 'DELETE'
      });
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      setPets(pets.filter(pet => pet.id !== id));
      toast.success('Data Delete Successfully', {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        });
    } catch(err) {
      console.error(err);
    }
  };
  

  const handleUpdate = (id) => {
    navigate(`/updatedataform/${id}`)
  };
 
    return (
  <>
    
    <table className="datatable">
        <thead>
            <tr>
                <th scope="col">Category</th>
                <th scope="col">Image</th>
                <th scope="col">Breed</th>
                <th scope="col">Age</th>
                <th scope="col">Vaccinated</th>
                <th scope="col">Location</th>
                <th scope="col">Status</th>
                <th scope="col">Price</th>
                <th scope="col">Screenshots</th>
                <th scope="col">Name</th>
                <th scope="col">Number</th>
                <th scope="col">Email</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
          {pets && pets.map((pet) => {
            return (
              <tr key={pet.id}>
              <td data-label='Category'>{pet.category}</td>
              <td data-label='Image'><img src={`http://localhost:8000/images/${pet.image}`} alt="Pet" /></td>
              <td data-label='Breed'>{pet.breed}</td>
              <td data-label='Age'>{pet.age}</td>
              <td data-label='Vaccinated'>{pet.vaccinated}</td>
              <td data-label='Location'>{pet.location}</td>
              <td data-label='Status'>{pet.status}</td>
              <td data-label='Price'>{pet.price}</td>
              <td data-label='Screenshots'><a href={`http://localhost:8000/images/${pet.screenshots}`} target="_blank"><img src={`http://localhost:8000/images/${pet.screenshots}`} alt=""/></a></td>
              <td data-label='Name'>{pet.name}</td>
              <td data-label='Number'>{pet.number}</td>
              <td data-label='Email'>{pet.email}</td> 
              <td data-label='Edit'><button className="edit_btn" onClick={() =>handleUpdate(pet.id)}>Edit</button></td>
              <td data-label='Delete'><button className="delete_btn" onClick={() => handleDelete(pet.id)}>Delete</button></td>
            </tr>
            )          
          })}         
        </tbody>
    </table>
</>
    );
  }
  export default DataDetails;