import { useContext, useState, useEffect} from 'react';
import { PetifyContext } from '../../context/context';
import { toast } from 'react-toastify';
import "./data_form.css";

function GalleryForm () {
  const [isActive, setIsActive] = useState(false);

  const handleIcon = () => {
    setIsActive(!isActive);
  };

  useEffect(() => {
    const handleScroll = () => {
      setIsActive(false);
    };
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const handleScroll = () => {
    setIsActive(false);
  };
  
  const [image, setImage] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('image', image);
    try{
      const response = await fetch('http://localhost:8000/gallery', {
        method: "Post",
        body: formData
      })
      // addPets(response.data.data.pets)
      toast.success('Image added Successfully', {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        })
      if (response.ok) {
        console.log('Pet uploaded successfully!');
      } else {
        console.error('Failed to upload pet.');
      }

    }catch (err){
      console.log(err)
    }
    setImage('')  
  };


  const handleImage = (e) => {
    setImage(e.target.files[0]);
   };


    return (
      <>
        <div className="wrapper">
          <form action="">
            <div className="title">
              Upload Images
            </div>
            <div className="form"> 
              <div className="inputfield">
                  <label>Image</label>
                  <input type="file" name='image' className="input" onChange={handleImage} />
              </div>  
              <div className="inputfield">
                <button 
                type="submit"
                className="btn" 
                onClick={handleSubmit}>Upload</button>
              </div>
            </div>
            </form>
        </div>
</>
    );
  }
  export default GalleryForm;