import React, { useState, useEffect} from 'react';
import './gallery.css';

import LightGallery from 'lightgallery/react';

// import styles
import 'lightgallery/css/lightgallery.css';
import 'lightgallery/css/lg-zoom.css';
import 'lightgallery/css/lg-thumbnail.css';

// import plugins if you need
import lgThumbnail from 'lightgallery/plugins/thumbnail';
import lgZoom from 'lightgallery/plugins/zoom';

const Gallery = () => {
  const [images, setImages] = useState([]);
  const [showMore, setShowMore] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:8000/gallery', {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          }
        });
        const data = await response.json();
        setImages(data.data.images);
      } catch (err) {
        console.log(err);
      }
    };
    fetchData();
  }, []);

  const handleImageClick = (image) => {
    window.open(image, '_blank');
  };

  const toggleShowMore = () => {
    setShowMore(!showMore);
  };

  const toggleShowLess = () => {
    setShowMore(false);
  };

  return (
    <section className="gallery" id="gallery">
      <div className="heading">
        <h3>our gallery</h3>
      </div>

      <div className="gallery-container">
        <LightGallery
          speed={500}
          plugins={[lgThumbnail, lgZoom]}
          subHtmlSelectorRelative={true}
        >
          {images.slice(0, showMore ? images.length : 6).map((img, index) => {
            return (
              <a key={index} className='box' href={`http://localhost:8000/gallery/${img.image}`} data-sub-html={`<h4>Image ${index + 1}</h4>`}>
                <img alt={`img${index}`} src={`http://localhost:8000/gallery/${img.image}`} />
              </a>
            );
          })}
        </LightGallery>
        
      </div>
      {images.length > 6 && (
          <div>
            {!showMore ? (
              <button className='btn' onClick={toggleShowMore}>Show More</button>
            ) : (
              <button className='btn' onClick={toggleShowLess}>Show Less</button>
            )}
          </div>
        )}
    </section>
  );
};

export default Gallery;
