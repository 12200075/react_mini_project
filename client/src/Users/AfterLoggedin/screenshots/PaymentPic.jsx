import React, { useState, useContext, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import './payments.css';
import { PetifyContext } from '../../../context/context';
import petifyapi from '../../../apis/petifyapi';
import {toast} from 'react-toastify';

import QR from './QR.png';

const PaymentPic = ({setAuth}) => {
  const navigate = useNavigate();
  const{id = " "} = useParams();
  const {pets, setPets} = useContext(PetifyContext);
  const [isActive, setIsActive] = useState(false);
  const [isActivePopUp, setIsActivePopUp] = useState(false);
  const [image, setImage] = useState(null);
  const [name, setName] = useState("");
  const [number, setNumber] = useState("");

  const toggleNavbar = () => {
    setIsActive(!isActive);
  };

  const closeNavbar = () => {
    setIsActive(false);
  };

  const showModal = () => {
    setIsActivePopUp(true);
  };

  const hideModal = () => {
    setIsActivePopUp(false);
  };

  const handleNav = () => {
    navigate('/user');
  };

  useEffect(() => {
    const handleBeforeUnload = (e) => {
      e.preventDefault();
      e.returnValue = '';
    };
  
    window.addEventListener('beforeunload', handleBeforeUnload);
  
    return () => {
      window.removeEventListener('beforeunload', handleBeforeUnload);
    };
  }, []);
  

  useEffect(() => {
    const fetchData = async () => {
      const response = await petifyapi.get(`/${id}`)
      setPets(response.data.data.pet)
    };
    fetchData() 
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('image', image);
    formData.append('name', name);
    formData.append('number', number);
    formData.append('email', sessionStorage.getItem('user_email'));
    const updatedPet = await fetch(`http://localhost:8000/data/payment/${id}`, {
      method:"PUT",
      body: formData
    });

    if(updatedPet.status === 200){
      navigate('/user');
      toast.success('Thank you, We will verify and Let you know soon', {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });      
    }else{
      const data = await updatedPet.json();
      toast.error(data, {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
        });   
  };
    }

  const handleImage = (e) => {
    setImage(e.target.files[0])
  };

  const logout = () => {
    localStorage.removeItem("token");
    setAuth(false);
    toast.success('Sign-Out Successfully', {
      position: "top-center",
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
      });
    };

  return (
    <>
    <section className="header">
        <a onClick={handleNav} style={{cursor: "pointer"}} className="logo">
          <i className="fas fa-paw"></i> Petify
        </a>

        <nav className={`navbar ${isActive ? 'active' : ''}`}>
          <a style={{cursor: "pointer"}} onClick={()=>{closeNavbar(); handleNav()}}>
            Home
          </a>
          <a style={{cursor: 'pointer'}} onClick={() => {closeNavbar(); logout()}}>
            Signout
          </a>
          
        </nav>

        <div
          id="menu-btn"
          className={`fas fa-bars ${isActive ? 'fa-times' : ''}`}
          onClick={toggleNavbar}
        ></div>
      </section>
    <section className={`payment ${isActivePopUp ? "active" : ""}`}>
    <div className="heading">
          <h3>Payment</h3>
          <p>Note: provride valid name and info,  pay half amount, visit store before 7 days from paymnet</p>
        </div>
        <form>
        <div className="box-container">
                    <div className="box">
                      <div className='scanbox'>
                        <div className="imageBox">
                          <span className='scanning'><b>Selected Pet</b></span><br/>
                          <img src={`http://localhost:8000/images/${pets.image}`} alt=''/>
                        </div>
                        <div className="imageBox">
                          <span className='scanning'><b>Scan Here</b></span><br/>
                          <img src={QR} alt=''/>
                        </div>
                        </div>
                    </div>
                    <div className="box">
                    <div className="inputBox">
                          <span><b>Full Name</b></span><br/>
                          <input type="text" value={name} onChange={(e)=>setName(e.target.value)} placeholder="enter your Name"/>
                        </div>
                    <div className="inputBox">
                            <span><b>Number</b></span><br/>
                            <input type="number" value={number} onChange={(e)=>setNumber(e.target.value)} placeholder="enter your Number"/>
                        </div>
                    </div>
                </div>
        <div className='con'>
            <label htmlFor="images" className="drop-container">
              <span className="drop-title">Drop Screenshot Here</span>
                or
              <input type="file" name='image' id='image' onChange={handleImage}  required/>
            </label>
            <button className='btn' onClick={ handleSubmit}>Submit</button>
        </div>
      <div className="payment-overlay" onClick={hideModal}></div>

        {isActivePopUp && (
        <div className="modal-box">
          <i className="fas fa-check-circle"></i>
          <h2>Thank You</h2>
          <h3>We will cross check and let you know as soon as possible</h3>
          <div className="button">
            <button className="btn" >
              Ok, Close
            </button>
          </div>
        </div>
      )}
      </form>
    </section>
    </>
  )
}

export default PaymentPic