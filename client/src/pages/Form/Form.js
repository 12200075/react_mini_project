import React from 'react';
import "./FormCSS.css";
import DataForm from '../../components/Form/data_form';
import GalleryForm from '../../components/Form/gallery_form';

const FormData = () => {
  return (
    <>
    <section className='form_container' id='form_id'>
      <DataForm/>
      <section className='form_container'>
        <GalleryForm/>
    </section>
    </section>
    
    </>
    
    
  )
}

export default FormData;
