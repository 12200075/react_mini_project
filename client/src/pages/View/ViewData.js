import React from 'react';
import "./ViewData.css";
import DataDetails from '../../components/Table/DataTable/data';
import ImageDetails from '../../components/Table/imageTable/Image';

const ViewData = () => {
  return (
    <>
    <section className="data" id="data">
    <div className="content">
            <h3>Uploaded Data</h3>
        </div>
       <DataDetails/>

       <section className="data" id="data">
          <div className="content">
            <h3>Uploaded Images</h3>
          </div>
          <ImageDetails/>
    </section>
    </section>
    </>
  )
}

export default ViewData;
